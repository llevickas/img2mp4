package main

import (
	"bytes"
	"html/template"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
	"github.com/icza/mjpeg"
)

func main() {
	os.MkdirAll("/tmp/img2mp4", 0777)

	http.HandleFunc("/", func(responseWriter http.ResponseWriter, request *http.Request) {
		render(responseWriter, "template/index.html", nil)
	})

	http.HandleFunc("/convert", func(responseWriter http.ResponseWriter, request *http.Request) {
		responseWriter.Header().Set("Content-Type", "application/json")
		err := request.ParseMultipartForm(8192 << 20)

		if err != nil {
			responseWriter.Write([]byte(err.Error()))
		}

		fps, err := strconv.Atoi(request.FormValue("fps"))
		showError(err, responseWriter)

		width, err := strconv.Atoi(request.FormValue("width"))
		showError(err, responseWriter)

		height, err := strconv.Atoi(request.FormValue("height"))
		showError(err, responseWriter)

		filename := string(time.Now().Format("2006_01_02_15_04_05"))+".mp4"
		aw, err := mjpeg.New("/tmp/img2mp4/"+filename, int32(width), int32(height), int32(fps))
		showError(err, responseWriter)

		for _, file := range request.MultipartForm.File["files"] {
			f, err := file.Open()

			defer f.Close()

			showError(err, responseWriter)

			buf := bytes.NewBuffer(nil)
			io.Copy(buf, f)

			showError(aw.AddFrame(buf.Bytes()), responseWriter)

		}

		showError(aw.Close(), responseWriter)
		responseWriter.Write([]byte("{\"video\": \"/file/"+filename+"\"}"))

		http.HandleFunc("/file/"+filename, func (responseWriter http.ResponseWriter, request *http.Request) {
			responseWriter.Header().Set("Content-Disposition", "attachment; filename="+strconv.Quote(filename))
			responseWriter.Header().Set("Content-Type", "application/octet-stream")
			http.ServeFile(responseWriter, request, "/tmp/img2mp4/"+filename)
		})
	})


	http.ListenAndServe(":8080", nil)
}

func render(writer http.ResponseWriter, filename string, data interface{}) {
	t := template.Must(template.ParseFiles(filename))
	t.Execute(writer, data)
}

func showError(err error, responseWriter http.ResponseWriter) {
	if err != nil {
		responseWriter.Write([]byte(err.Error()))
		return
	}
}
